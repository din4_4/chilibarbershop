<!DOCTYPE html>
<html <?php get_language_attributes(); ?>>

<head>
    <meta charset=<?php bloginfo('charset'); ?>>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <!-- CSS only -->
    <?php wp_head(); ?>

</head>

<body>
    <!-- header -->
    <header class="header container">

        <div class="header__block header__block_left">
            <nav class="menu header-menu">
                <?php
                wp_nav_menu([
                    'theme_location' => 'top_menu_left',
                    'menu' => '',
                    'container' => false,
                    'menu_class' => 'menu__list',
                ]);
                ?>
            </nav>
            <a href="<?php echo get_site_url() . "/certificates/"; ?>" class="bg-present"></a>
        </div>

        <div class="header__block header__block_center">
            <a href="<?php echo get_site_url(); ?>" class="header-logo__link">
                <img src="<?php echo get_template_directory_uri() . "/assets/image/logo.png"; ?>" alt=""
                    class="header-logo__img">
            </a>
        </div>

        <div class="header__block header__block_right">
            <nav class="menu header-menu">
                <?php
                wp_nav_menu([
                    'theme_location' => 'top_menu_right',
                    'menu' => '',
                    'container' => false,
                    'menu_class' => 'menu__list',
                ]);
                ?>
                <div class="header__button">
                    <a target="_blank" href="https://b74898.yclients.com/company/90215/menu?o=" class="button">Записаться</a>
                </div>
            </nav>
            <i class="mobile-menu-open bg-menu_open" id="mobile-menu-open"></i>
        </div>

    </header>

    <!-- mobile-menu -->
    <div class="mobile-menu container">
        <div class="mobile-menu__head">
            <a href="<?php echo get_site_url(); ?>" class="mobile-menu-logo__link">
                <img src="<?php echo get_template_directory_uri() . "/assets/image/logo-dark.png"; ?>" alt="" class="
                    mobile-menu-logo__img">
            </a>
            <i class="mobile-menu-close bg-menu_close" id="mobile-menu-close"></i>
        </div>
        <nav class="menu">
            <?php
            wp_nav_menu([
                'theme_location' => 'mobile_menu',
                'menu' => '',
                'container' => false,
                'menu_class' => 'menu__list',
            ]);
            ?>
        </nav>
        <div class="mobile-menu__button">
            <a target="_blank" href="https://b74898.yclients.com/company/90215/menu?o=" class="button">Записаться</a>
        </div>
    </div>