<?php get_header(); ?>

<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_template_directory_uri() . "/assets/image/bg.webp"; ?>);">
    <div class="header-bg__info-page container">
        <h1 class="header-bg__title">
            Информация отсутсвует
        </h1>
        <p class="header-bg__subtitle">
            Но не отчаивайтесь, она скоро появится и вы найдете что искали.
        </p>
    </div>
</div>

<?php get_footer(); ?>