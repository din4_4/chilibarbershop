<?php
/*
Template Name: services
*/
?>

<?php get_header(); ?>


<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_the_post_thumbnail_url(get_queried_object_id(), 'full') ?>);">
    <div class="header-bg__info-page container">
        <h1 class="header-bg__title">
            <?echo CFS()->get('title');?>
        </h1>
        <p class="header-bg__subtitle">
            <?echo CFS()->get('subtitle');?>
        </p>
    </div>
    <a href="#start-content" class="header-bg__arrow">
        <img src="<?php echo get_template_directory_uri() . "/assets/image/icons/icon-arrow-down.png"; ?>" alt=""
            class="header-bg__img">
    </a>
</div>

<!-- services -->
<div id="start-content" class="services container-full">
    <h3 class="services__title">услуги</h3>
    <div class="services__block">
        <div class="services__selection">
            <ul class="selection__list">
                <?php
                global $post;
                $index = 0;
                $myposts = get_posts([
                    'numberposts' => 15,
                    'post_type' => 'service-list'
                ]);

                if ($myposts) {
                    foreach ($myposts as $post) {
                        setup_postdata($post);
                        $index += 1;
                ?>

                <li class="selection__elem">
                    <a href="<?php echo "#service" . $index; ?>" class="selection__link">
                        <?php echo get_the_title(); ?>
                    </a>
                </li>

                <?php
                    }
                } else {
                }
                wp_reset_postdata(); // Сбрасываем $post
                ?>
            </ul>
        </div>

        <div class="services__list">
            <?php
            global $post;
            $index = 0;
            $myposts = get_posts([
                'numberposts' => 15,
                'post_type' => 'service-list'
            ]);

            if ($myposts) {
                foreach ($myposts as $post) {
                    setup_postdata($post);
                    $index += 1;
            ?>
            <div class="service">
                <a name="<?php echo 'service' . $index; ?>" href="<?php echo CFS()->get('service_link'); ?>"
                    target="_blank" class="service__title">
                    <?php echo get_the_title(); ?>
                </a>
                <?php $service_details = CFS()->get('service_list');
                    foreach ($service_details as $row) { ?>
                <div class="service-elem">
                    <div class="service-elem__line">
                        <h6 class="service-elem__title">
                            <?php echo $row['service_title']; ?>
                        </h6>
                        <span class="service-elem__price">
                            <?php echo $row['service_price']; ?>
                        </span>
                    </div>
                    <?php if ($row['service_description'] != "") { ?>
                    <p class="service-elem__subtitle">
                        <?php echo $row['service_description']; ?>
                    </p>
                    <?php } ?>
                </div>
                <?php }
                ?>
            </div>

            <?php
                }
            } else {
            }
            wp_reset_postdata(); // Сбрасываем $post
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>