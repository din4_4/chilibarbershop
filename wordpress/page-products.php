<?php
/*
Template Name: products
*/
?>

<?php get_header(); ?>


<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_the_post_thumbnail_url(get_queried_object_id(), 'full') ?>);">
    <div class="header-bg__info-page container">
        <h1 class="header-bg__title">
            <?echo CFS()->get('title');?>
        </h1>
        <p class="header-bg__subtitle">
            <?echo CFS()->get('subtitle');?>
        </p>
    </div>
    <a href="#start-content" class="header-bg__arrow">
        <img src="<?php echo get_template_directory_uri() . "/assets/image/icons/icon-arrow-down.png"; ?>" alt=""
            class="header-bg__img">
    </a>
</div>

<!-- products -->
<div id="start-content" class="products container-full">

    <?php
    global $post;

    $myposts = get_posts([
        'numberposts' => 10,
        'post_type' => 'product-list'
    ]);

    if ($myposts) {
        foreach ($myposts as $post) {
            setup_postdata($post);
    ?>

    <div class="product">
        <div class="product_top">
            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="product__img">
            <p class="product__subtitle">
                <?php echo CFS()->get('subtitle'); ?>
            </p>
            <h6 class="product__title">
                <?php echo CFS()->get('title'); ?>
            </h6>
        </div>
        <span class="product__price">
            <?php echo CFS()->get('price'); ?>
        </span>
    </div>

    <?php
        }
    } else {
    }
    wp_reset_postdata(); // Сбрасываем $post
    ?>

</div>

<?php get_footer(); ?>