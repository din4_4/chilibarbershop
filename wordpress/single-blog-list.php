<?php get_header(); ?>

<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_the_post_thumbnail_url(get_queried_object_id(), 'full') ?>);">
</div>

<!-- blog-preview -->
<div class="blog-preview container-full">
    <div class="blog-preview__content">
        <?php echo get_the_content(); ?>
        <div>
            <p class="author">Автор:
                <?php echo CFS()->get('author'); ?>
            </p>
            <span class="date">
                <?php echo get_the_date(); ?>
            </span>
        </div>
    </div>
</div>

<!-- review -->
<div class="review container">
    <div class="review__content">
        <h5 class="review__title">Оставить отзыв</h5>
        <div class="review__form">
            <?php echo do_shortcode('[contact-form-7 id="139" title="Отзывы"]'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>