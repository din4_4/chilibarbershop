<?php
/*
Template Name: home
*/
?>

<?php get_header(); ?>

<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_the_post_thumbnail_url(get_queried_object_id(), 'full') ?>);">
    <div class="header-bg__info-page container">
        <h1 class="header-bg__title">
            <?echo CFS()->get('title');?>
        </h1>
        <p class="header-bg__subtitle">
            <?echo CFS()->get('subtitle');?>
        </p>
    </div>
    <a href="#start-content" class="header-bg__arrow">
        <img src="<?php echo get_template_directory_uri() . "/assets/image/icons/icon-arrow-down.png"; ?>" alt=""
            class="header-bg__img">
    </a>
</div>

<!-- why-us -->
<div id="start-content" class="why-us container-full">
    <h3 class="why-us__title">
        <?php echo CFS()->get('why_are_we_title'); ?>
    </h3>
    <div class="why-us__list">
        <?php $block = CFS()->get('why_are_we_list');
        $index = 1;
        foreach ($block as $row) { ?>
        <div class="why-us__elem">
            <span class="why-us__number">
                <?php echo '0' . $index; ?>
            </span>
            <p class="why-us__info">
                <?php echo $row['why_are_we_text']; ?>
            </p>
        </div>
        <?php
            $index += 1;
        }
        ?>
    </div>
</div>

<!-- services -->
<div class="services container-full">
    <h3 class="services__title">услуги</h3>
    <div class="services__block">
        <div class="services__selection">
            <ul class="selection__list">
                <?php
                global $post;
                $index = 0;
                $myposts = get_posts([
                    'numberposts' => 15,
                    'post_type' => 'service-list'
                ]);

                if ($myposts) {
                    foreach ($myposts as $post) {
                        setup_postdata($post);
                        $index += 1;
                ?>

                <li class="selection__elem">
                    <a href="<?php echo "#service" . $index; ?>" class="selection__link">
                        <?php echo get_the_title(); ?>
                    </a>
                </li>

                <?php
                    }
                } else {
                }
                wp_reset_postdata(); // Сбрасываем $post
                ?>
            </ul>
        </div>

        <div class="services__list">
            <?php
            global $post;
            $index = 0;
            $myposts = get_posts([
                'numberposts' => 15,
                'post_type' => 'service-list'
            ]);

            if ($myposts) {
                foreach ($myposts as $post) {
                    setup_postdata($post);
                    $index += 1;
            ?>
            <div class="service">
                <a name="<?php echo 'service' . $index; ?>" href="<?php echo CFS()->get('service_link'); ?>"
                    target="_blank" class="service__title">
                    <?php echo get_the_title(); ?>
                </a>
                <?php $service_details = CFS()->get('service_list');
                    foreach ($service_details as $row) { ?>
                <div class="service-elem">
                    <div class="service-elem__line">
                        <h6 class="service-elem__title">
                            <?php echo $row['service_title']; ?>
                        </h6>
                        <span class="service-elem__price">
                            <?php echo $row['service_price']; ?>
                        </span>
                    </div>
                    <?php if ($row['service_description'] != "") { ?>
                    <p class="service-elem__subtitle">
                        <?php echo $row['service_description']; ?>
                    </p>
                    <?php } ?>
                </div>
                <?php }
                ?>
            </div>

            <?php
                }
            } else {
            }
            wp_reset_postdata(); // Сбрасываем $post
            ?>
        </div>
    </div>
</div>

<!-- masters -->
<div class="masters container-full">
    <div class="masters__head">
        <h3 class="masters__title"><?php echo  CFS()->get('masters_title');?></h3>
        <p class="masters__subtitle"><?php echo  CFS()->get('masters_subtitle');?></p>
    </div>
    <div class="masters__list">
        <?php
        global $post;

        $myposts = get_posts([
            'numberposts' => 3,
            'post_type' => 'master-list'
        ]);

        if ($myposts) {
            foreach ($myposts as $post) {
                setup_postdata($post);
        ?>

        <a href="<?php echo get_permalink(); ?>" class="master">
            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="master__img">
            <p class="master__subtitle">
                <?php echo get_the_content(); ?>
            </p>
            <h5 class="master__title">
                <?php echo get_the_title(); ?>
            </h5>
        </a>

        <?php
            }
        } else {
        }
        wp_reset_postdata(); // Сбрасываем $post
        ?>
    </div>
</div>

<!-- block-info -->
<div class="block-info-custom container-full">
    <p class="block-info__text block-info__text_left"><?php echo  CFS()->get('info-block_text_left');?></p>
    <img src="<?php echo get_template_directory_uri() . "/assets/image/logo.png" ?>" alt="" class="block-info__img">
    <p class="block-info__text block-info__text_right"><?php echo  CFS()->get('info-block_text_right');?></p>
    <p class="block-info__text"><?php echo  CFS()->get('info-block_text');?></p>
</div>

<?php get_footer(); ?>