<!-- footer -->
<footer class="footer">
    <div class="footer__top container">
        <div class="footer__column">
            <a href="<?php echo get_home_url(); ?>" class="footer__logo-link">Чили барбершоп</a>
        </div>
        <div class="footer__info">
            <div class="footer__menu-group">
                <div class="footer__column">
                    <h5 class="footer__title">Барбершоп</h5>
                    <nav class="menu footer-menu">
                        <?php
                        wp_nav_menu([
                            'theme_location' => 'footer_menu',
                            'menu' => '',
                            'container' => false,
                            'menu_class' => 'menu__list',
                        ]);
                        ?>
                    </nav>
                </div>
                <div class="footer__column">
                    <h5 class="footer__title">Социальные сети</h5>
                    <nav class="menu footer-menu">
                        <?php
                        wp_nav_menu([
                            'theme_location' => 'social_menu',
                            'menu' => '',
                            'container' => false,
                            'menu_class' => 'menu__list',
                        ]);
                        ?>
                    </nav>
                </div>
            </div>
            <div class="footer__column footer__contacts">
                <h5 class="footer__title">контакты</h5>
                <div class="contacts">
                    <div class="contact">
                        <i class="contact__icon bg-map"></i>
                        <address class="contact__address">Беларусь, Витебск,<br>улица Калинина, 16</address>
                    </div>
                    <div class="contact">
                        <i class="contact__icon bg-phone"></i>
                        <div class="contact__phones">
                            <a class="contact__phone" href="tel:+375 212 663777">+375 212 663777</a>
                            <a class="contact__phone" href="tel:+375 33 6037773">+375 33 6037773 (МТС)</a>
                            <a class="contact__phone" href="tel:+375 29 3627773">+375 29 3627773 (Velcom)</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <p class="footer__privacy">© 2007-2022 chili. All Rights Reserved. Privacy Policy.</p>
    </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>