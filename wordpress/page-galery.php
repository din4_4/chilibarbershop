<?php
/*
Template Name: galery
*/
?>

<?php get_header(); ?>

<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_the_post_thumbnail_url(get_queried_object_id(), 'full') ?>);">
    <div class="header-bg__info-page container">
        <h1 class="header-bg__title">
            <?echo CFS()->get('title');?>
        </h1>
        <p class="header-bg__subtitle">
            <?echo CFS()->get('subtitle');?>
        </p>
    </div>
    <a href="#start-content" class="header-bg__arrow">
        <img src="<?php echo get_template_directory_uri() . "/assets/image/icons/icon-arrow-down.png"; ?>" alt=""
            class="header-bg__img">
    </a>
</div>

<!-- galery -->
<div id="start-content" class="galery container-full">
    <?php
    $categories = get_post_gallery($post->ID, false);
    $categories = explode(',', $categories['ids']);
    $page = isset($_GET['cpage']) ? abs((int) $_GET['cpage']) : 1; // слово в адресе
    $numOfItems = 12; // количество постов на странице
    $to = $page * $numOfItems; // 
    $current = $to - $numOfItems; // 
    $total = sizeof($categories); // всего 
    
    $pagination = paginate_links(
        array(
            'base' => add_query_arg('cpage', '%#%'),
            // слово в адресе
            'format' => '',
            'end_size' => 4,
            'prev_next' => true,
            'prev_text' => __(''),
            'next_text' => __(''),
            'total' => ceil($total / $numOfItems),
            'current' => $page,
            'always_prev_next_batton' => 'true',
            'type' => 'list'
        )
    );

    $index = 0;

    for ($i = $current; $i < $to; ++$i) {
        if (isset($categories[$i])) {
            $category = $categories[$i];
        } else {
            $category = '';
        }
        if ($category == '') {
            continue;
        } // оборвать показ изображений - уже пусто
    
        $attachment = get_post($category);

        //тут мы уже знаем что картинка есть
        $index += 1;

        if ($index == 1) {?>
            <div class="galery__line">
                <img src="<?php echo $attachment->guid;?>" alt="" class="galery__img">
        <? }
        if ($index == 2) {?>
                <img src="<?php echo $attachment->guid;?>" alt="" class="galery__img">
        <? }
        if ($index==3) {?>
            <img src="<?php echo $attachment->guid;?>" alt="" class="galery__img">
            </div>
        <?
            $index=0;
            continue;
        }
    }
    if($index!=0){?>
        </div>

        <?}?>


        <nav class="pagination">
        <?
            if ($pagination !== NULL) {
                echo $pagination;
            } else {
                echo '';
            }
        ?>
        </nav>

</div>




<?php get_footer(); ?>