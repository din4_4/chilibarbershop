<?php

// отладочная функция для вывода в консоль
function console_log($data)
{
	if (is_array($data) || is_object($data)) {
		echo ("<script>console.log('php_array: " . json_encode($data) . "');</script>");
	} else {
		echo ("<script>console.log('php_string: " . $data . "');</script>");
	}
}


// подключение css
add_action('wp_enqueue_scripts', function () {
	wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.min.css');
	wp_enqueue_style('font-mulish', 'https://fonts.googleapis.com/css?family=Mulish');
	wp_enqueue_style('font-jura', 'https://fonts.googleapis.com/css?family=Jura');
});

// подключение js
add_action('wp_footer', function () {
	wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0.0', true);
});


add_theme_support('post-thumbnails');
add_theme_support('title-tag');
add_theme_support('custom-logo');
add_theme_support('menus');
add_theme_support('widgets');
add_theme_support('title-tag');
add_theme_support('post-formats', array(''));

// регистрируем меню
add_action('after_setup_theme', function () {
	register_nav_menu('top_menu_left', 'Меню в шапке слева');
	register_nav_menu('top_menu_right', 'Меню в шапке справа');
	register_nav_menu('footer_menu', 'Меню футер');
	register_nav_menu('mobile_menu', 'Мобильное меню');
	register_nav_menu('social_menu', 'Социальные сети');
});

// настройки кастомных классов для меню
function special_nav_class($classes, $item)
{
	$classes[] = "menu__elem";
	if (in_array('current-menu-item', $classes)) {
		$classes[] = 'menu__elem_active ';
	}
	return $classes;
}

add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function add_specific_menu_location_atts($atts, $item, $args)
{
	$atts['class'] = 'menu__link';
	return $atts;
}
add_filter('nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3);


// регистрируем новый тип записей для мастеров, услуг, блога
add_action('init', 'register_post_types');
function register_post_types()
{
	register_post_type('master-list', [
		'label' => null,
		'labels' => [
			'name' => 'Мастера',
			// основное название для типа записи
			'singular_name' => 'Мастер',
			// название для одной записи этого типа
			'add_new' => 'Добавить мастера',
			// для добавления новой записи
			'add_new_item' => 'Добавление мастера',
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item' => 'Редактирование мастера',
			// для редактирования типа записи
			'new_item' => 'Новый',
			// текст новой записи
			'view_item' => 'Смотреть',
			// для просмотра записи этого типа.
			'search_items' => 'Искать',
			// для поиска по этим типам записи
			'not_found' => 'Не найдено',
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине',
			// если не было найдено в корзине
			'parent_item_colon' => '',
			// для родителей (у древовидных типов)
			'menu_name' => 'Мастера',
			// название меню

		],
		'description' => '',
		'public' => true,
		'register_post_type' => false,
		'show_in_menu' => true,
		// показывать ли в меню адмнки
		'show_in_admin_bar' => true,
		// зависит от show_in_menu
		'show_in_rest' => null,
		// добавить в REST API. C WP 4.7
		'rest_base' => null,
		// $post_type. C WP 4.7
		'menu_position' => 4,
		'menu_icon' => 'dashicons-admin-users',
		'hierarchical' => false,
		'supports' => ['title', 'editor', 'thumbnail'],
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies' => [],
		'has_archive' => false,
		'rewrite' => array('slug' => 'masters', 'with_front' => false),
		'query_var' => true,
	]);

	register_post_type('service-list', [
		'label' => null,
		'labels' => [
			'name' => 'Услуги',
			// основное название для типа записи
			'singular_name' => 'Услуга',
			// название для одной записи этого типа
			'add_new' => 'Добавить услугу',
			// для добавления новой записи
			'add_new_item' => 'Добавление услуги',
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item' => 'Редактирование услуги',
			// для редактирования типа записи
			'new_item' => 'Новое',
			// текст новой записи
			'view_item' => 'Смотреть',
			// для просмотра записи этого типа.
			'search_items' => 'Искать',
			// для поиска по этим типам записи
			'not_found' => 'Не найдено',
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине',
			// если не было найдено в корзине
			'parent_item_colon' => '',
			// для родителей (у древовидных типов)
			'menu_name' => 'Услуги',
			// название меню

		],
		'description' => '',
		'public' => true,
		'register_post_type' => false,
		'show_in_menu' => true,
		// показывать ли в меню адмнки
		'show_in_admin_bar' => true,
		// зависит от show_in_menu
		'show_in_rest' => null,
		// добавить в REST API. C WP 4.7
		'rest_base' => null,
		// $post_type. C WP 4.7
		'menu_position' => 4,
		'menu_icon' => 'dashicons-list-view',
		'hierarchical' => false,
		'supports' => ['title', 'editor', 'thumbnail'],
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies' => [],
		'has_archive' => false,
		'rewrite' => array('slug' => 'services', 'with_front' => false),
		'query_var' => true,
		'pages' => true
	]);

	register_post_type('blog-list', [
		'label' => null,
		'labels' => [
			'name' => 'Блог',
			// основное название для типа записи
			'singular_name' => 'Статья',
			// название для одной записи этого типа
			'add_new' => 'Добавить статью',
			// для добавления новой записи
			'add_new_item' => 'Добавление статьи',
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item' => 'Редактирование статьи',
			// для редактирования типа записи
			'new_item' => 'Новое',
			// текст новой записи
			'view_item' => 'Смотреть',
			// для просмотра записи этого типа.
			'search_items' => 'Искать',
			// для поиска по этим типам записи
			'not_found' => 'Не найдено',
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине',
			// если не было найдено в корзине
			'parent_item_colon' => '',
			// для родителей (у древовидных типов)
			'menu_name' => 'Блог',
			// название меню

		],
		'description' => '',
		'public' => true,
		'register_post_type' => false,
		'show_in_menu' => true,
		// показывать ли в меню адмнки
		'show_in_admin_bar' => true,
		// зависит от show_in_menu
		'show_in_rest' => null,
		// добавить в REST API. C WP 4.7
		'rest_base' => null,
		// $post_type. C WP 4.7
		'menu_position' => 4,
		'menu_icon' => 'dashicons-format-aside',
		'hierarchical' => false,
		'supports' => ['title', 'editor', 'thumbnail'],
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies' => [],
		'has_archive' => false,
		'rewrite' => array('slug' => 'blog', 'with_front' => false),
		'query_var' => true,
		'pages' => true
	]);

	register_post_type('product-list', [
		'label' => null,
		'labels' => [
			'name' => 'Товары',
			// основное название для типа записи
			'singular_name' => 'Товары',
			// название для одной записи этого типа
			'add_new' => 'Добавить товар',
			// для добавления новой записи
			'add_new_item' => 'Добавление товара',
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item' => 'Редактирование товара',
			// для редактирования типа записи
			'new_item' => 'Новое',
			// текст новой записи
			'view_item' => 'Смотреть',
			// для просмотра записи этого типа.
			'search_items' => 'Искать',
			// для поиска по этим типам записи
			'not_found' => 'Не найдено',
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине',
			// если не было найдено в корзине
			'parent_item_colon' => '',
			// для родителей (у древовидных типов)
			'menu_name' => 'Товары',
			// название меню

		],
		'description' => '',
		'public' => true,
		'register_post_type' => false,
		'show_in_menu' => true,
		// показывать ли в меню адмнки
		'show_in_admin_bar' => true,
		// зависит от show_in_menu
		'show_in_rest' => null,
		// добавить в REST API. C WP 4.7
		'rest_base' => null,
		// $post_type. C WP 4.7
		'menu_position' => 4,
		'menu_icon' => 'dashicons-database',
		'hierarchical' => false,
		'supports' => ['title', 'thumbnail'],
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies' => [],
		'has_archive' => false,
		'rewrite' => array('slug' => 'products', 'with_front' => false),
		'query_var' => true,
		'pages' => true
	]);

	register_post_type('certificates-list', [
		'label' => null,
		'labels' => [
			'name' => 'Сертификаты',
			// основное название для типа записи
			'singular_name' => 'Сертификат',
			// название для одной записи этого типа
			'add_new' => 'Добавить сертификат',
			// для добавления новой записи
			'add_new_item' => 'Добавление сертификата',
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item' => 'Редактирование сертификата',
			// для редактирования типа записи
			'new_item' => 'Новое',
			// текст новой записи
			'view_item' => 'Смотреть',
			// для просмотра записи этого типа.
			'search_items' => 'Искать',
			// для поиска по этим типам записи
			'not_found' => 'Не найдено',
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине',
			// если не было найдено в корзине
			'parent_item_colon' => '',
			// для родителей (у древовидных типов)
			'menu_name' => 'Сертификаты',
			// название меню

		],
		'description' => '',
		'public' => true,
		'register_post_type' => false,
		'show_in_menu' => true,
		// показывать ли в меню адмнки
		'show_in_admin_bar' => true,
		// зависит от show_in_menu
		'show_in_rest' => null,
		// добавить в REST API. C WP 4.7
		'rest_base' => null,
		// $post_type. C WP 4.7
		'menu_position' => 4,
		'menu_icon' => 'dashicons-admin-page',
		'hierarchical' => false,
		'supports' => ['title', 'thumbnail'],
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies' => [],
		'has_archive' => false,
		'rewrite' => array('slug' => 'certificates', 'with_front' => false),
		'query_var' => true,
		'pages' => true
	]);
}

?>