<?php
	echo '<div class="pagination">';
    $big = 999999999;
    echo paginate_links( 
        array(
            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'current' => max( 1, get_query_var('paged') ),
            'total'   => $wp_query->max_num_pages,
            'prev_text'    => __(''),
            'next_text'    => __(''),
        ) 
    );
    echo '</div>';
?>