window.onload = function () {
  // обработка открытия закрытия мобильного меню

  function mobile_menu(e) {
    try {
      mobileOpenId = "mobile-menu-open";
      mobileCloseId = "mobile-menu-close";
      mobileMenuClass = "mobile-menu";
      mobileMenuClassOpen = mobileMenuClass + "_open";
      mobileMenu = document.getElementsByClassName(mobileMenuClass)[0];
      if (
        !e.target.classList.contains(mobileMenuClass) ||
        e.target.id == mobileCloseId
      ) {
        mobileMenu.classList.remove(mobileMenuClassOpen);
      }
      if (e.target.id == mobileOpenId) {
        mobileMenu.classList.add(mobileMenuClassOpen);
      }
    } catch (error) {}
  }

  document.onclick = function (e) {
    mobile_menu(e);
  };
  
};

