<?php
/*
Template Name: about-us
*/
?>

<?php get_header(); ?>

<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_the_post_thumbnail_url(get_queried_object_id(), 'full') ?>);">
    <div class="header-bg__info-page container">
        <h1 class="header-bg__title">
            <?echo CFS()->get('title');?>
        </h1>
        <p class="header-bg__subtitle">
            <?echo CFS()->get('subtitle');?>
        </p>
    </div>
    <a href="#start-content" class="header-bg__arrow">
        <img src="<?php echo get_template_directory_uri() . "/assets/image/icons/icon-arrow-down.png"; ?>" alt=""
            class="header-bg__img">
    </a>
</div>

<!-- why-us -->
<div id="start-content" class="why-us why-us-xxl container-full">
    <h3 class="why-us__title">
        <?php echo CFS()->get('why_are_we_title'); ?>
    </h3>
    <div class="why-us__list">
        <?php $block = CFS()->get('why_are_we_list');
        $index = 1;
        foreach ($block as $row) { ?>
        <div class="why-us__elem">
            <span class="why-us__number">
                <?php echo '0' . $index; ?>
            </span>
            <p class="why-us__info">
                <?php echo $row['why_are_we_text']; ?>
            </p>
        </div>
        <?php
            $index += 1;
        }
        ?>
    </div>
</div>

<!-- collage -->
<div class="collage container-full">
    <?php $loop = CFS()->get('photos');
    foreach ($loop as $row) {
        if ($row['photo']!=""){?>

    <img src="<?php echo $row['photo'];?>" alt="" class="collage__img">
    <?
        }}
    ?>
</div>

<!-- masters -->
<div class="masters container-full">
    <div class="masters__head">
        <h3 class="masters__title">
            <?php echo  CFS()->get('masters_title');?>
        </h3>
        <p class="masters__subtitle">
            <?php echo  CFS()->get('masters_subtitle');?>
        </p>
    </div>
    <div class="masters__list">
        <?php
            global $post;

            $myposts = get_posts([ 
                'numberposts' => 3,
                'post_type'    => 'master-list'
            ]);

            if( $myposts ){
                foreach( $myposts as $post ){
                    setup_postdata( $post );
            ?>

        <a href="<?php echo get_permalink(); ?>" class="master">
            <img src="<?php echo get_the_post_thumbnail_url();?>" alt="" class="master__img">
            <p class="master__subtitle">
                <?php echo get_the_content(); ?>
            </p>
            <h5 class="master__title">
                <?php echo get_the_title(); ?>
            </h5>
        </a>

        <?php 
                }
            } else {}
            wp_reset_postdata(); // Сбрасываем $post
        ?>
    </div>
</div>

<!-- block-info -->
<div class="block-info container-full">
    <img src="<?php echo get_template_directory_uri() . "/assets/image/logo.png" ?>" alt="" class="block-info__img">
    <p class="block-info__text">
        <?echo CFS()->get('text-info');?>
    </p>
</div>

<?php get_footer(); ?>