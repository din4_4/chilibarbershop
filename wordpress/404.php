<?php get_header(); ?>

<!-- header background -->
<div class="header-bg"
    style="background-image: url(<?php echo get_template_directory_uri() . "/assets/image/bg.webp"; ?>);">
    <div class="header-bg__info-page container">
        <h1 class="header-bg__title">
            Ошибка 404
        </h1>
        <p class="header-bg__subtitle">
            Такой страницы не существует. <br>Попробуйте пока перейти в другие разделы нашего сайта.
        </p>
    </div>
</div>

<?php get_footer(); ?>