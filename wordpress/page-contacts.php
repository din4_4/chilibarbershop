<?php
/*
Template Name: contacts
*/
?>

<?php get_header(); ?>

<!-- contacts-page -->

<div class="contacts-page">
    <div class="contacts__content container">
        <div class="contacts__detail">
            <div class="contacts-info">
                <div class="contacts__block">
                    <h6 class="contacts__title">контакты</h6>
                    <div class="contact">
                        <i class="contact__icon bg-phone"></i>
                        <div class="contact__phones">
                            <?echo CFS()->get('phones');?>
                        </div>
                    </div>
                </div>
                <div class="contacts__block">
                    <h6 class="contacts__title">часы работы</h6>
                    <p class="contacts__subtitle">
                        <?echo CFS()->get('working_hours');?>
                    </p>
                </div>

                <div class="contacts__block">
                    <h6 class="contacts__title">Где мы находимся</h6>
                    <div class="contact">
                        <i class="contact__icon bg-map"></i>
                        <address class="contact__address">
                            <?echo CFS()->get('address');?>
                        </address>
                    </div>
                </div>
            </div>
            <div class="contacts__map">
                <iframe src=" <?echo CFS()->get('map_link');?>" width="100%" height="100%" style="border:0;"
                    allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
        <div class="connect-with-us">
            <h5 class="connect-with-us__title">
                <?echo CFS()->get('title');?>
            </h5>
            <p class="connect-with-us__subtitle">
                <?echo CFS()->get('subtitle');?>
            </p>
            <div class="connect-with-us__form">
                 <?php echo do_shortcode('[contact-form-7 id="136" title="Связаться с нами"]');?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>